package nsu.upprpo.kharisov.zhuk.consoleui;

import nsu.upprpo.kharisov.zhuk.abstractui.AbstractUI;
import nsu.upprpo.kharisov.zhuk.canonyzer.Canonyzer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConsoleUI implements AbstractUI {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    @Override
    public void start() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String timestamp = dateFormat.format(new Date(System.currentTimeMillis()));
            System.out.print(timestamp + " : Enter polynom: ");
            String in;
            while ((in = br.readLine()) != null) {
                timestamp = dateFormat.format(new Date(System.currentTimeMillis()));
                System.out.println(timestamp + " : " + Canonyzer.canonyze(in));
                timestamp = dateFormat.format(new Date(System.currentTimeMillis()));
                System.out.print(timestamp + " : Enter polynom: ");
            }
        } catch (IOException i) {
            //TODO log exception somehow
            System.err.println("IO exception");
            System.exit(1);
        }
    }
}
