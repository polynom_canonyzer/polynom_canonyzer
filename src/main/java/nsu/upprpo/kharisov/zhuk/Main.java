package nsu.upprpo.kharisov.zhuk;

import nsu.upprpo.kharisov.zhuk.abstractui.AbstractUI;
import nsu.upprpo.kharisov.zhuk.consoleui.ConsoleUI;
import nsu.upprpo.kharisov.zhuk.graphicalui.GraphicalUI;
import org.apache.commons.cli.*;

public class Main {

    public static void main(String[] args) {
        Options options = new Options();

        Option helpOpt = new Option("h", "help", false, "Show help message");
        options.addOption(helpOpt);

        Option uiOpt = new Option("g", "graphical-ui", false, "Toggle graphical ui.");
        options.addOption(uiOpt);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;
        AbstractUI ui = null;

        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("help")) {
                formatter.printHelp("java -jar polynom_canonyzer.jar", options);
                System.exit(0);
            } else if (cmd.hasOption("graphical-ui")) {
                ui = new GraphicalUI();
            } else {
                ui = new ConsoleUI();
            }
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("java -jar polynom_canonyzer.jar", options);
            System.exit(1);
        }
        ui.start();
    }
}
