package nsu.upprpo.kharisov.zhuk.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import nsu.upprpo.kharisov.zhuk.canonyzer.Simplifier;

public class Term implements Comparable<Term> {
    private int coefficient = 1;
    private List<Factor> factors = new ArrayList<>();

    public Term(Factor f, int coefficient){
        factors.add(f);
        this.coefficient = coefficient;
    }

    private Term(int coefficient){
        this.coefficient = coefficient;
    }

    public void add(Factor f){
        factors.add(f);
    }

    int getCoefficient() {
        return coefficient;
    }
    void setCoefficient(int coefficient) {
        this.coefficient = coefficient;
    }

    public List<Factor> getFactors() {
        return factors;
    }

    public void add(Term term){
        int coef1 = this.getCoefficient();
        if (this.factors.get(0).getType() == FactorType.NUM){
            coef1 *= ((AtomNumber)this.factors.get(0).getValue()).getValue();
        }
        int coef2 = term.getCoefficient();
        if (term.getFactors().get(0).getType() == FactorType.NUM){
            coef2 *= ((AtomNumber)term.getFactors().get(0).getValue()).getValue();
        }
        int general = coef1 + coef2;
        int newcoef = 1;
        if (general != 0) newcoef = (coef1 + coef2)/Math.abs(coef1 + coef2);
        general *= newcoef;
        AtomNumber a = new AtomNumber(general);
        Factor f = new Factor(a);
        f.setType(FactorType.NUM);
        if (this.factors.get(0).getType() == FactorType.NUM){
            this.factors.remove(0);
        }
        this.factors.add(0, f);
        this.setCoefficient(newcoef);
    }

    public static Term multiplication(Term t1, Term t2){
        int c = t1.getCoefficient()*t2.getCoefficient();
        Term newTerm = new Term(c);
        for (Factor f : t1.getFactors()){
            newTerm.add(new Factor(f));
        }
        for (Factor f : t2.getFactors()){
            newTerm.add(new Factor(f));
        }
        newTerm.simplify();
        return newTerm;
    }

    public void simplify(){
        int length = factors.size();
        for (int i = 0; i < length; i++) {
            Factor f = factors.get(i);
            if (f.getType() == FactorType.EXPR){
                Simplifier.simplify(((AtomExpr)f.getValue()).getExpr());
                if (f.getDegree() != 1){
                    ((AtomExpr)f.getValue()).getExpr().setTerms(Expression.degree(((AtomExpr)f.getValue()).getExpr(), f.getDegree()));
                }
            }
        }
        for (int i = 0; i < length; i++) {
            Factor f1 = factors.get(i);
            for (int j = 0; j < length; j++) {
                if (i == j) continue;
                Factor f2 = factors.get(j);
                if (f1.getType() == f2.getType()){
                    if (f1.getType() == FactorType.NUM){
                        int valuef1 = ((AtomNumber)f1.getValue()).getValue();
                        int valuef2 = ((AtomNumber)f2.getValue()).getValue();
                        f1.setValue(new AtomNumber(valuef1*valuef2));
                        factors.remove(f2);
                        j--;
                        length--;
                    } else if (f1.getType() == FactorType.VAR){
                        char valuef1 = ((AtomVariable)f1.getValue()).getVariable();
                        char valuef2 = ((AtomVariable)f2.getValue()).getVariable();
                        if (valuef1 == valuef2){
                            f1.setDegree(f2.getDegree()+f1.getDegree());
                            factors.remove(f2);
                            j--;
                            length--;
                        }
                    } else{
                        ((AtomExpr)f1.getValue()).getExpr().setTerms(Expression.multiplication(((AtomExpr)f1.getValue()).getExpr(), ((AtomExpr)f2.getValue()).getExpr()));
                        factors.remove(f2);
                        j--;
                        length--;
                    }
                }
            }
        }
        Term simpleTerm = new Term(this.getCoefficient());
        Expression expression = null;
        Factor finalFactor;
        for (int i = 0; i < length; i++){
            Factor f = factors.get(i);
            if (f.getType() == FactorType.VAR || f.getType() == FactorType.NUM){
                if (f.getType() == FactorType.VAR && f.getDegree() == 0) {
                    factors.remove(f);
                    i--;
                    length --;
                    continue;
                }
                simpleTerm.add(f);
            } else expression = ((AtomExpr)f.getValue()).getExpr();
        }
        if (expression != null) {
            Expression finalResult = new Expression();
            for (Term t : expression.getTerms()) {
                finalResult.add(multiplication(simpleTerm, t));
            }
            this.factors.clear();
            finalFactor = new Factor(new AtomExpr(finalResult), 1);
            finalFactor.setType(FactorType.EXPR);
            this.factors.add(finalFactor);
        } else if (simpleTerm.getFactors().size() == 0){
            this.factors.clear();
            AtomNumber a = new AtomNumber(1);
            finalFactor = new Factor(a, 1);
            finalFactor.setType(FactorType.NUM);
            this.factors.add(finalFactor);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Term term = (Term) o;
        return coefficient == term.coefficient &&
                Objects.equals(factors, term.factors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coefficient, factors);
    }

    @Override
    public int compareTo(Term compared){
        if ((this.factors.size() == 1 && this.factors.get(0).getType() == FactorType.NUM) &&
                (compared.getFactors().size() == 1 && compared.getFactors().get(0).getType() == FactorType.NUM)) {
            return 0;
        }
        if (this.factors.size() == 1 && this.factors.get(0).getType() == FactorType.NUM) {
            return 1;
        }
        if (compared.getFactors().size() == 1 && compared.getFactors().get(0).getType() == FactorType.NUM) {
            return -1;
        }
        int lengthCompared = compared.getFactors().size();
        int length = factors.size();
        int start = 0;
        int startCompared = 0;
        if (this.factors.get(0).getType() == FactorType.NUM) {
            start = 1;
            length--;
        }
        if (compared.getFactors().get(0).getType() == FactorType.NUM){
            startCompared = 1;
            lengthCompared--;
        }
        int minLength = length > lengthCompared ? lengthCompared : length;
        for (int i = start, j = startCompared; i < minLength+start; i++, j++) {
            Factor f1 = factors.get(i);
            Factor f2 = compared.getFactors().get(j);
            if (f1.getType() == FactorType.NUM && f2.getType() == FactorType.NUM) continue;
            if (f1.compareTo(f2) == 1){
                return 1;
            } else if (f1.compareTo(f2) == -1){
                return -1;
            }
        }
        if (lengthCompared == length){
            return 0;
        } if (length > lengthCompared){
            return 1;
        } else {
            return -1;
        }
    }

    String toStr(){
        StringBuilder string = new StringBuilder();
        for (Factor f : factors){
            if (f.getType() == FactorType.NUM){
                int value = ((AtomNumber)f.getValue()).getValue();
                if (value != 1 || factors.size() == 1) string.append(Integer.toString(value));
                if (f.getDegree() > 1) string.append('^').append(Integer.toString(f.getDegree()));
            } else if (f.getType() == FactorType.VAR){
                char value = ((AtomVariable)f.getValue()).getVariable();
                string.append(value);
                if (f.getDegree() > 1) string.append('^').append(Integer.toString(f.getDegree()));
            } else {
                string.append(((AtomExpr) f.getValue()).getExpr().toStr());
            }
        }
        return string.toString();
    }
}
