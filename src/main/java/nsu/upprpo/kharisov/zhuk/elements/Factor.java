package nsu.upprpo.kharisov.zhuk.elements;


import java.util.Objects;

public class Factor implements Comparable<Factor>{
    private int degree = 1;
    private FactorType type = null;
    private Atom atom;

    public Factor(Atom a){
        atom = a;
    }

    public Factor(Atom a, int d){
        atom = a;
        degree = d;
    }

    Factor(Factor copyFrom) {
        this.degree = copyFrom.getDegree();
        this.type = copyFrom.getType();
        if (copyFrom.getType() == FactorType.NUM){
            int value = ((AtomNumber)copyFrom.getValue()).getValue();
            this.atom = new AtomNumber(value);
        } else if (copyFrom.getType() == FactorType.VAR){
            char value = ((AtomVariable)copyFrom.getValue()).getVariable();
            this.atom = new AtomVariable(value);
        } else {
            Expression value = ((AtomExpr)copyFrom.getValue()).getExpr();
            this.atom = new AtomExpr(new Expression(value));
        }
    }

    public FactorType getType() {
        return type;
    }
    public void setType(FactorType type) {
        this.type = type;
        if (type == FactorType.NUM && degree != 1){
            int value = ((AtomNumber)atom).getValue();
            atom = new AtomNumber((int)Math.pow(value,degree));
            degree = 1;
        }
    }

    public Atom getValue(){
        return atom;
    }
    void setValue(Atom a){
        atom = a;
    }

    public int getDegree() {
        return degree;
    }
    void setDegree(int d){
        degree = d;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, atom, degree);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Factor factor = (Factor) obj;
        return this.compareTo(factor) == 0;
    }

    @Override
    public int compareTo(Factor f2) {
        if (f2 == null) throw new NullPointerException();
        if (this.getType() == FactorType.NUM && f2.getType() == FactorType.NUM){
            return Integer.compare(((AtomNumber)this.getValue()).getValue(), ((AtomNumber)f2.getValue()).getValue());
        } else if (this.getType() == FactorType.NUM && f2.getType() == FactorType.VAR) {
            return -1;
        } else if (this.getType() == FactorType.VAR && f2.getType() == FactorType.NUM) {
            return 1;
        } else if (this.getType() == FactorType.VAR && f2.getType() == FactorType.VAR){
            char c = ((AtomVariable)this.getValue()).getVariable();
            char cCompared  = ((AtomVariable)f2.getValue()).getVariable();
            if (c > cCompared){
                return 1;
            } else if (c == cCompared){
                return Integer.compare(f2.getDegree(), this.getDegree());
            } else return -1;
        }
        return -1;
    }
}
