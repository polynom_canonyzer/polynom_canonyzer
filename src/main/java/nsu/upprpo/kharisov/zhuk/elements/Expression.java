package nsu.upprpo.kharisov.zhuk.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Expression {
    private List<Term> terms = new ArrayList<>();

    @Override
    public int hashCode() {
        return terms.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Expression that = (Expression) o;
        return Objects.equals(terms, that.terms);
    }

    public Expression(Term t){
        terms.add(t);
    }

    public Expression(List<Term> terms){
        this.terms = terms;
    }

    Expression(Expression copyFrom){
        this.terms = new ArrayList<>(copyFrom.getTerms());
    }

    Expression(){}

    public void add(Term t){
        terms.add(t);
    }

    public List<Term> getTerms() {
        return terms;
    }
    void setTerms(List<Term> newTerms){
        terms = newTerms;
    }

    static List<Term> multiplication(Expression expr1, Expression expr2){
        List<Term> e1Terms = expr1.getTerms();
        List<Term> e2Terms = expr2.getTerms();
        List<Term> result = new ArrayList<>();
        for (Term t1 : e1Terms){
            for (Term t2 : e2Terms){
                result.add(Term.multiplication(t1, t2));
            }
        }
        return result;
    }

    static List<Term> degree(Expression expr, int degree){
        List<Term> firstDegree = new ArrayList<>(expr.getTerms());
        Expression firstDegreeExpression = new Expression(firstDegree);
        List<Term> result = new ArrayList<>(expr.getTerms());
        Expression resultExpression;
        if (degree == 0){
            AtomNumber a = new AtomNumber(1);
            Factor f = new Factor(a,1);
            f.setType(FactorType.NUM);
            Term t = new Term(f, 1);
            result.clear();
            result.add(t);
        } else {
            for (int i = 1; i < degree; i++){
                resultExpression = new Expression(result);
                result = multiplication(resultExpression, firstDegreeExpression);
            }}
        return result;
    }

    public String toStr(){
        StringBuilder string = new StringBuilder();
        for (Term t : terms){
            if (string.length() == 0){
                if (t.getCoefficient() < 0) string.append("-").append(t.toStr());
                else  string.append(t.toStr());
            } else {
                if (t.getCoefficient() < 0) string.append("-").append(t.toStr());
                else  string.append("+").append(t.toStr());
            }
        }
        return string.toString();
    }
}
