package nsu.upprpo.kharisov.zhuk.elements;


public class AtomVariable extends Atom {
    private char variable;

    @Override
    public int hashCode() {
        return Character.hashCode(variable);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AtomVariable that = (AtomVariable) o;
        return variable == that.variable;
    }

    public AtomVariable(char v){
        variable = v;
    }

    char getVariable() {
        return variable;
    }
}
