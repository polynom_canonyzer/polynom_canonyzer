package nsu.upprpo.kharisov.zhuk.elements;


public class AtomNumber extends Atom {
    private int value;

    @Override
    public int hashCode() {
        return Integer.hashCode(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AtomNumber that = (AtomNumber) o;
        return value == that.value;
    }

    public AtomNumber(int v){
        value = v;
    }

    public int getValue() {
        return value;
    }
}
