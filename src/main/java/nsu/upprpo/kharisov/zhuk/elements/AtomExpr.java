package nsu.upprpo.kharisov.zhuk.elements;


import java.util.Objects;

public class AtomExpr extends Atom {
    private Expression expr;

    @Override
    public int hashCode() {
        return expr.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AtomExpr atomExpr = (AtomExpr) o;
        return Objects.equals(expr, atomExpr.expr);
    }

    public AtomExpr(Expression e){
        expr = e;
    }

    public Expression getExpr() {
        return expr;
    }
}