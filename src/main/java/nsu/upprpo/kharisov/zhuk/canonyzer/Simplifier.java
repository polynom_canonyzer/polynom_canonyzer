package nsu.upprpo.kharisov.zhuk.canonyzer;

import nsu.upprpo.kharisov.zhuk.elements.*;
import java.util.List;


public class Simplifier {

    private Simplifier(){}

    public static void simplify(Expression expr){
        List<Term> terms = expr.getTerms();
        for (Term t : terms){
            t.simplify();
        }
    }

}
