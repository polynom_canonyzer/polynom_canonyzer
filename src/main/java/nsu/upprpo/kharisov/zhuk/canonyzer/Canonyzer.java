package nsu.upprpo.kharisov.zhuk.canonyzer;

import nsu.upprpo.kharisov.zhuk.elements.Expression;
import nsu.upprpo.kharisov.zhuk.lexer.Lexer;
import nsu.upprpo.kharisov.zhuk.parser.Parser;
import nsu.upprpo.kharisov.zhuk.exceptions.PolynomialException;

public abstract class Canonyzer {

    public static String canonyze(String inputExpression){
        try {
            Sorter.clean();// just clean all static fields for tests or multiusages of this function
            Parser parser = new Parser(inputExpression);
            Expression e = parser.parse(); // returning ready tree where expression - {(-/+term) + (-/+term)..} and term - {factor * factor..}
            //UNCOMMENT/COMMENT NEXT LINE FOR "a^35-a^35" -> "ERROR: TOO BIG"
            if (parser.isBigDegree()) throw new IllegalArgumentException("Big degree");
            Simplifier.simplify(e);
            Sorter.collectTerms(e);
            Sorter.sort();
            e = new Expression(Sorter.lead());
            String result = e.toStr();
            if (result.length() == 0) result = "0";
            return result;
        } catch (PolynomialException polynomialException) {
            return polynomialException.getInfo();
        } catch (IllegalArgumentException err){
            return "Degree of entered expression is too big.";
        }
//        return "imagine being simplified expression!";
    }
}
