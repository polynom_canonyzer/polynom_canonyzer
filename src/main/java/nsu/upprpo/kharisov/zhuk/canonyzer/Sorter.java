package nsu.upprpo.kharisov.zhuk.canonyzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nsu.upprpo.kharisov.zhuk.elements.*;

public class Sorter {
    private static List<Term> result = new ArrayList<>();
    private static boolean singleton = true;

    static void clean(){
        result.clear();
        singleton = true;
    }

    /**
     *  we collect all terms from tree in one array.
     *  because of our array is static it is allowed only one external usage of collect() method
     *  but there is the way to use it again after clean()
     * */
    static void collectTerms(Expression expression){
        if (singleton){
            collect(expression);
            singleton = false;
        }
    }

    private static void collect(Expression expression){
        boolean join;
        for (Term term : expression.getTerms()){
            join = true;
            List<Factor> factors = term.getFactors();
            for (Factor factor : factors){
                if (factor.getType() == FactorType.EXPR){
                    join = false;
                    collect(((AtomExpr)factor.getValue()).getExpr());
                }
            }
            if (join) result.add(term);
        }
    }

    /**
     * sort factors in all terms ba -> ab
     * */
    static void sort(){
        for (Term term : result){
            Collections.sort(term.getFactors());
        }
    }

    /**
     * leading all terms and sort them  z-2z+a -> a-z
     * */
    static List<Term> lead(){
        int length = result.size();
        int degree;
        int value;
        boolean removed;
        for (int i = 0; i < length; i++) {
            Term t1 = result.get(i);
            removed = false;
            for (int j = 0; j < length; j++) {
                if (i == j) continue;
                Term t2 = result.get(j);
                if (t1.compareTo(t2) == 0){
                    t1.add(t2);
                    result.remove(t2);
                    length--;
                    j--;
                }
            }
            if (t1.getFactors().get(0).getType() == FactorType.NUM){
                value = ((AtomNumber)t1.getFactors().get(0).getValue()).getValue();
                if (value == 0){
                    result.remove(t1);
                    length--;
                    i--;
                    removed = true;
                }else if (value == 1 && t1.getFactors().size() != 1){
                    t1.getFactors().remove(0);
                }
            }
            if ( ! removed){
                degree = 0;
                for (Factor f : t1.getFactors()) {
                    degree += f.getDegree();
                    if (degree > 32){
                        throw new NumberFormatException("Big degree.");
                    }
                }
            }
        }
        Collections.sort(result);
        return result;
    }


}
