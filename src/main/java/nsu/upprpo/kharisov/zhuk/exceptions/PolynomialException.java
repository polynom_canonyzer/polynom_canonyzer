package nsu.upprpo.kharisov.zhuk.exceptions;

public class PolynomialException  extends Exception {
    public PolynomialException() { super(); }
    public PolynomialException(String message) { super(message); }
    public PolynomialException(String message, Throwable cause) { super(message, cause); }
    public PolynomialException(Throwable cause) { super(cause); }
    public String getInfo(){
        return super.getMessage();
    }
}