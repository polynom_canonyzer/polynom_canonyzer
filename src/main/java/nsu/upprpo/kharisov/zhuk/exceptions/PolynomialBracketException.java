package nsu.upprpo.kharisov.zhuk.exceptions;

public class PolynomialBracketException extends PolynomialException {
    public PolynomialBracketException() {
        super("Wrong enter: you entered invalid bracket symbol. Every opening bracket must be followed by closing one.");
    }
}