package nsu.upprpo.kharisov.zhuk.exceptions;

public class PolynomialSymbolException extends PolynomialException {
    public PolynomialSymbolException() {
        super("Wrong enter: you entered invalid symbol. There are no symbols except numbers, lowercase letters" +
                " and '+', '-', '^', ')', '(' allowed.");
    }
}