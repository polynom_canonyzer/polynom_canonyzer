package nsu.upprpo.kharisov.zhuk.exceptions;

public class PolynomialSpaceException extends PolynomialException {
    public PolynomialSpaceException() {
        super("Wrong enter: you entered invalid space symbol. There are no space symbols between numbers allowed.");
    }
}
