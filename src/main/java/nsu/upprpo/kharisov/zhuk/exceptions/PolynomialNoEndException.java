package nsu.upprpo.kharisov.zhuk.exceptions;

public class PolynomialNoEndException extends PolynomialException {
    public PolynomialNoEndException() {
        super("Wrong enter: the end of entering was not found or it was incorrect.");
    }
}
