package nsu.upprpo.kharisov.zhuk.exceptions;

public class PolynomialEmptyException extends PolynomialException {
    public PolynomialEmptyException() {
        super("Wrong enter: the enter is empty.");
    }
}