package nsu.upprpo.kharisov.zhuk.graphicalui;

import nsu.upprpo.kharisov.zhuk.abstractui.AbstractUI;
import nsu.upprpo.kharisov.zhuk.canonyzer.Canonyzer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GraphicalUI extends JFrame implements AbstractUI {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm ");
    private transient ExecutorService executor = Executors.newSingleThreadExecutor();
    private JTextField inField;
    private JTextArea outField;
    private JButton canonyzeButton;


    public GraphicalUI() {
        // Frame parameters
        setSize(500, 800);
        setLocationByPlatform(true);
        setTitle("Polynom Canonyzer 0.1a");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Menu bar initialization
        JMenuBar menuBar = new JMenuBar();
        JMenu helpMenu = new JMenu("Help");
        JMenuItem aboutMenuItem = new JMenuItem("About");
        aboutMenuItem.addActionListener(new InfoMenuItemListener("Simple polynom canonyzer tool\n" +
                "Created by Julia Zhuk and Damir Kharisov"));
        helpMenu.add(aboutMenuItem);
        JMenuItem usageMenuItem = new JMenuItem("Usage");
        usageMenuItem.addActionListener(new InfoMenuItemListener("Enter your polynom into text field " +
                "and click the button"));
        helpMenu.add(usageMenuItem);
        menuBar.add(helpMenu);
        setJMenuBar(menuBar);

        // Content pane initialization
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weighty = 1;
        c.weightx = 1;
        c.insets = new Insets(0, 50, 0, 50);
        c.fill = GridBagConstraints.HORIZONTAL;
        inField = new JTextField("Enter polynom..", 300);
        inField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (inField.getText().equals("Enter polynom..")) {
                    inField.setText("");
                    inField.setForeground(Color.BLACK);
                }
            }
            @Override
            public void focusLost(FocusEvent e) {
                if (inField.getText().isEmpty()) {
                    inField.setForeground(Color.GRAY);
                    inField.setText("Enter polynom..");
                }
            }
        });
        panel.add(inField, c);
        c.gridy = 1;
        canonyzeButton = new JButton("Canonyze");
        canonyzeButton.addActionListener(new CanonyzeButtonListener());
        panel.add(canonyzeButton, c);
        c.gridy = 2;
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(0, 0, 0, 0);
        outField = new JTextArea();
        outField.setMargin(new Insets(0, 20, 20, 20));
        outField.setBackground(null);
        outField.setEditable(false);
        outField.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
        JScrollPane scrollPane = new JScrollPane(outField, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(15, 0));
        scrollPane.getHorizontalScrollBar().setPreferredSize(new Dimension(0, 15));
        scrollPane.setBorder(null);
        panel.add(scrollPane, c);
        setContentPane(panel);
    }

    private String readIn() {
        return inField.getText();
    }

    private void showOut(String out) {
        String timestamp = dateFormat.format(new Date(System.currentTimeMillis()));
        outField.append(timestamp + ": " + out + "\n");
    }

    class CanonyzeButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String in = readIn();
            executor.execute(() -> {
                canonyzeButton.setEnabled(false);
                String out = Canonyzer.canonyze(in);
                showOut(out);
                canonyzeButton.setEnabled(true);
            });
        }
    }

    class InfoMenuItemListener implements ActionListener {
        private String message;

        InfoMenuItemListener(String message) {
            this.message = message;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(GraphicalUI.this,
                    message, "About", JOptionPane.PLAIN_MESSAGE);
        }
    }

    @Override
    public void start() {
        setVisible(true);
    }
}
