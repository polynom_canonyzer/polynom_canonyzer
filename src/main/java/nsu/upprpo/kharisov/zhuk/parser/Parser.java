package nsu.upprpo.kharisov.zhuk.parser;

import nsu.upprpo.kharisov.zhuk.lexer.*;
import nsu.upprpo.kharisov.zhuk.elements.*;
import java.io.IOException;
import nsu.upprpo.kharisov.zhuk.exceptions.*;

public class Parser {
    private Lexer lexer;
    private Lexeme currentLexeme;
    private boolean bigDegree = false;

    public Parser(String inputExpression) throws PolynomialException {
        lexer = new Lexer(inputExpression);
        currentLexeme = lexer.getLexeme();
    }

    public Expression parse() throws PolynomialException {
        Expression result = parseExpression();
        if (currentLexeme.getType() == LexemeType.EOF){
            return result;
        } else {
            throw new PolynomialNoEndException();
        }
    }

    private Expression parseExpression() throws PolynomialException {
        Term tmp;
        if (currentLexeme.getType() == LexemeType.MINUS){
            currentLexeme = lexer.getLexeme();
            tmp = parseTerm(-1);
        } else {
            tmp = parseTerm(1);
        }
        Expression result = new Expression(tmp);
        while (currentLexeme.getType() == LexemeType.PLUS || currentLexeme.getType() == LexemeType.MINUS) {
            if (currentLexeme.getType() == LexemeType.PLUS) {
                currentLexeme = lexer.getLexeme();
                result.add(parseTerm(1));
            } else {
                currentLexeme = lexer.getLexeme();
                result.add(parseTerm(-1));
            }
        }
        return result;
    }

    private Term parseTerm(int coefficient) throws PolynomialException {
        FactorType type;
        if (currentLexeme.getType() == LexemeType.NUMBER){
            type = FactorType.NUM;
        } else if (currentLexeme.getType() == LexemeType.VARIABLE){
            type = FactorType.VAR;
        } else {
            type = FactorType.EXPR;
        }
        Factor tmp = parseFactor();
        tmp.setType(type);
        Term result = new Term(tmp, coefficient);
        while (currentLexeme.getType() == LexemeType.NUMBER || currentLexeme.getType() == LexemeType.VARIABLE || currentLexeme.getType() == LexemeType.LEFT_PAREN) {
            if (currentLexeme.getType() == LexemeType.NUMBER){
                type = FactorType.NUM;
            } else if (currentLexeme.getType() == LexemeType.VARIABLE){
                type = FactorType.VAR;
            } else {
                type = FactorType.EXPR;
            }
            tmp = parseFactor();
            tmp.setType(type);
            result.add(tmp);
        }
        return result;
    }

    private Factor parseFactor() throws PolynomialException {
        Atom a = parseAtom();
        Factor result = new Factor(a);
        if (currentLexeme.getType() == LexemeType.CARET) {
            currentLexeme = lexer.getLexeme();
            try {
                int degree = Integer.parseInt(currentLexeme.getLexemeText());
                result = new Factor(a, degree);
                if (degree > 32) setBigDegree(true);
            } catch (IllegalArgumentException err){
                throw new PolynomialSymbolException();
            }
            currentLexeme = lexer.getLexeme();
        }
        return result;
    }

    private Atom parseAtom() throws PolynomialException {
        if (currentLexeme.getType() == LexemeType.NUMBER) {
            int tmp = Integer.parseInt(currentLexeme.getLexemeText());
            currentLexeme = lexer.getLexeme();
            return new AtomNumber(tmp);
        } else if (currentLexeme.getType() == LexemeType.VARIABLE) {
            char tmp = currentLexeme.getLexemeText().charAt(0);
            currentLexeme = lexer.getLexeme();
            return new AtomVariable(tmp);
        } else if (currentLexeme.getType() == LexemeType.LEFT_PAREN) {
            currentLexeme = lexer.getLexeme();
            Expression tmp = parseExpression();
            if (currentLexeme.getType() == LexemeType.RIGHT_PAREN) {
                currentLexeme = lexer.getLexeme();
                return new AtomExpr(tmp);
            } else
                throw new PolynomialSymbolException();
        } else {
            throw new PolynomialBracketException();
        }
    }

    public boolean isBigDegree() {
        return bigDegree;
    }

    private void setBigDegree(boolean bigDegree) {
        this.bigDegree = bigDegree;
    }
}
