package nsu.upprpo.kharisov.zhuk.lexer;


public enum LexemeType {
    PLUS, MINUS, CARET, LEFT_PAREN, RIGHT_PAREN, NUMBER, VARIABLE, EOF
}