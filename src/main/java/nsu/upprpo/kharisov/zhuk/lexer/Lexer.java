package nsu.upprpo.kharisov.zhuk.lexer;


import nsu.upprpo.kharisov.zhuk.exceptions.PolynomialEmptyException;
import nsu.upprpo.kharisov.zhuk.exceptions.PolynomialException;
import nsu.upprpo.kharisov.zhuk.exceptions.PolynomialSpaceException;
import nsu.upprpo.kharisov.zhuk.exceptions.PolynomialSymbolException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Lexer {
    private String input;
    private int counter = 0;
    private int currentChar;

    public Lexer(String input) throws PolynomialException {
        this.input = validate(input);
        readChar();
    }

    private void readChar() {
        if (counter == input.length()){
            currentChar = -1;
            return;
        }
        currentChar = input.charAt(counter);
        counter++;
    }

    public Lexeme getLexeme() throws PolynomialException {
        Lexeme tmp;
        switch (currentChar) {
            case '+':
                tmp = new Lexeme(LexemeType.PLUS, "+");
                readChar();
                break;
            case '-':
                tmp = new Lexeme(LexemeType.MINUS, "-");
                readChar();
                break;
            case '^':
                tmp = new Lexeme(LexemeType.CARET, "^");
                readChar();
                break;
            case '(':
                tmp = new Lexeme(LexemeType.LEFT_PAREN, "(");
                readChar();
                break;
            case ')':
                tmp = new Lexeme(LexemeType.RIGHT_PAREN, ")");
                readChar();
                break;
            case -1:
                tmp = new Lexeme(LexemeType.EOF, null);
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                StringBuilder builder = new StringBuilder();
                builder.append((char)currentChar);
                readChar();
                while (Character.isDigit((char)currentChar)) {
                    builder.append((char)currentChar);
                    readChar();
                }
                tmp = new Lexeme(LexemeType.NUMBER, builder.toString());
                if (Character.isWhitespace((char)currentChar))
                    readChar();
                break;
            default:
                tmp = new Lexeme(LexemeType.VARIABLE, Character.toString((char)currentChar));
                readChar();
                break;
        }
        return tmp;
    }

    private static String validate(String input) throws PolynomialException{
        input = check(input);
        if (input.length() != 0)
            return input;
        throw new PolynomialEmptyException();
    }

    private static String check(String input) throws PolynomialException{
        Pattern invalidSpace = Pattern.compile("^.*[0-9] [0-9].*$");
        Matcher check = invalidSpace.matcher(input);
        if (check.matches()){
            throw new PolynomialSpaceException();
        }
        input = clean(input);
        Pattern invalidSymbols = Pattern.compile("^([a-z]*|[0-9]*|[-+^()]*)*$");
        check = invalidSymbols.matcher(input);
        if (!check.matches()){
            throw new PolynomialSymbolException();
        }
        return input;
    }

    private static String clean(String input){
        return input.replaceAll("\\s", "");
    }
}
