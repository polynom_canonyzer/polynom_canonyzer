package nsu.upprpo.kharisov.zhuk.canonyzer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCanonyzer {

    @Test
    void basicTest() {
        try {
            assertEquals("a", Canonyzer.canonyze("a"));
            assertEquals("-ab", Canonyzer.canonyze("-ab"));
            assertEquals("-6ab", Canonyzer.canonyze("-2a3b"));
            assertEquals("a^2+b^2-c^2", Canonyzer.canonyze("a^2+b^2-c^2"));
            assertEquals("abd^2+2abde^2+2abdf^9+abe^4+2abe^2f^9+" +
                            "abf^18+acd^2+2acde^2+2acdf^9+ace^4+2ace^2f^9+acf^18",
                    Canonyzer.canonyze("a(b+c)(  d+ e    ^2+(f^9))^2"));
            assertEquals("23e+23f", Canonyzer.canonyze("(e+f)23"));
        } catch (Exception e) {
            System.err.println("Exception in unexpected place during test execution");
            e.printStackTrace();
        }
    }

    @Test
    void monomialTest() {
        try {
            assertEquals("abc", Canonyzer.canonyze("bca"));
            assertEquals("2abc", Canonyzer.canonyze("bca2"));
            assertEquals("2ab", Canonyzer.canonyze("ab + ab"));
            assertEquals("0", Canonyzer.canonyze("ab - ab"));
            assertEquals("-ab", Canonyzer.canonyze("ab - a2b"));
            assertEquals("a+b", Canonyzer.canonyze("b + a"));
            assertEquals("ab+ac", Canonyzer.canonyze("ac + ab"));
            assertEquals("ab^2+ab", Canonyzer.canonyze("ab + abb"));
            assertEquals("ab+abc", Canonyzer.canonyze("abc + ab"));
            assertEquals("ab", Canonyzer.canonyze("ab + ac - ac"));
        } catch (Exception e) {
            System.err.println("Exception in unexpected place during test execution");
            e.printStackTrace();
        }
    }

    @Test
    void trickyTest() {
        try {
            assertEquals("-2a", Canonyzer.canonyze("a(-2)"));
            assertEquals("2a", Canonyzer.canonyze("a(-2)(-1)"));
            assertEquals("4a", Canonyzer.canonyze("a(-2)^2"));
            assertEquals("-4a", Canonyzer.canonyze("(-a)(-2)^2"));
            assertEquals("-8a", Canonyzer.canonyze("a(-2)^3"));
            assertEquals("a", Canonyzer.canonyze("a(-2)^0"));
            assertEquals("1", Canonyzer.canonyze("(-2)^0"));
            assertEquals("1", Canonyzer.canonyze("(a+b+d+c+t)^0"));

            assertEquals("a^32", Canonyzer.canonyze("(((((a^2)^2)^2)^2)^2)"));

            assertEquals("ab", Canonyzer.canonyze("ba"));
            assertEquals("abcdefghijklmnopqrstuvwxyz", Canonyzer.canonyze("zyxwvutsrqponmlkjihgfebcda"));
            assertEquals("a+b+c+d+e+f+g+h+i+j+k+l+m+n+o+p+q+r+s+t+u+v+w+x+y+z", Canonyzer.canonyze("z+y+x+w+v+u+t+s+r+q+p+o+n+m+l+k+j+i+h+g+f+e+b+c+d+a"));
            assertEquals("a^5+a^4+a^3+a^2+a", Canonyzer.canonyze("a+aa+aaa+aaaa+aaaaa"));
            assertEquals("a^15z+a^5f+a^4e+a^3d+a^2c+ab", Canonyzer.canonyze("ab+a^2c+a^3d+a^4e+a^5f+a^15z"));
            assertEquals("a^11+z^32", Canonyzer.canonyze("z^32+a^11"));
        } catch (Exception e) {
            System.err.println("Exception in unexpected place during test execution");
            e.printStackTrace();
        }
    }

    @Test
    void arithmeticTest() {
        try {
            assertEquals("a+4", Canonyzer.canonyze("2 + a + 2"));
            assertEquals("4", Canonyzer.canonyze("(2)2"));
            assertEquals("b", Canonyzer.canonyze("0a + 1b + 0^2"));
            assertEquals("a+b", Canonyzer.canonyze("a^0(a+b)"));
            assertEquals("a+ab", Canonyzer.canonyze("a(a^0+b)"));
            assertEquals("72", Canonyzer.canonyze("2^3+4^3"));
            assertEquals("8253472", Canonyzer.canonyze("224^3-12^6+32"));
            assertEquals("-1", Canonyzer.canonyze("2-3"));
            assertEquals("0", Canonyzer.canonyze("1-1"));
        } catch (Exception e) {
            System.err.println("Exception in unexpected place during test execution");
            e.printStackTrace();
        }
    }

    @Test
    void exponentTest() {
        try {
            assertEquals("a^3", Canonyzer.canonyze("aaa"));
            assertEquals("a^2+2ab+b^2", Canonyzer.canonyze("(a+b)^2"));
            assertEquals("a^2-2ab+b^2", Canonyzer.canonyze("(a-b)^2"));
            assertEquals("a^3+3a^2b+3ab^2+b^3", Canonyzer.canonyze("(a+b)^3"));
            assertEquals("a^3-3a^2b+3ab^2-b^3", Canonyzer.canonyze("(a-b)^3"));
            assertEquals("a^32", Canonyzer.canonyze("a^16a^16"));
            assertEquals("ac", Canonyzer.canonyze("ab^0c"));
            assertEquals("b+1", Canonyzer.canonyze("b + 0^0"));
            assertEquals("b+1", Canonyzer.canonyze("b + a^0"));
            assertEquals("b-1", Canonyzer.canonyze("b - 0^0"));
            assertEquals("b-1", Canonyzer.canonyze("b - a^0"));

        } catch (Exception e) {
            System.err.println("Exception in unexpected place during test execution");
            e.printStackTrace();
        }
    }

    @Test
    void parenthesesTest() {
        try {
            assertEquals("a^2+2ab^2+b^4", Canonyzer.canonyze("((a)+(b)^2)^2"));
            assertEquals("a", Canonyzer.canonyze("(((((a)))))"));
            assertEquals("a^2", Canonyzer.canonyze("(a)a"));
        } catch (Exception e) {
            System.err.println("Exception in unexpected place during test execution");
            e.printStackTrace();
        }
    }

    @Test
    void whitespaceTest() {
        try {
            assertEquals("a^2+2ab+b^2", Canonyzer.canonyze("(    a +\nb  )\n^\n2\n"));
        } catch (Exception e) {
            System.err.println("Exception in unexpected place during test execution");
            e.printStackTrace();
        }
    }
}
