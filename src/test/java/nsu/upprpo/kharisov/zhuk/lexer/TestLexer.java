package nsu.upprpo.kharisov.zhuk.lexer;

import org.junit.jupiter.api.Test;
import nsu.upprpo.kharisov.zhuk.exceptions.PolynomialException;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TestLexer {
    @Test
    void testGetLexemePlus() {
        try {
            Lexer lexer = new Lexer("+");
            Lexeme currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.PLUS, currentLexeme.getType());
            assertEquals("+", currentLexeme.getLexemeText());
        } catch (PolynomialException i) {
            i.printStackTrace();
        }
    }

    @Test
    void testGetLexemeMinus() {
        try {
            Lexer lexer = new Lexer("-");
            Lexeme currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.MINUS, currentLexeme.getType());
            assertEquals("-", currentLexeme.getLexemeText());
        } catch (PolynomialException i) {
            i.printStackTrace();
        }
    }

    @Test
    void testGetLexemeCaret() {
        try {
            Lexer lexer = new Lexer("^");
            Lexeme currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.CARET, currentLexeme.getType());
            assertEquals("^", currentLexeme.getLexemeText());
        } catch (PolynomialException i) {
            i.printStackTrace();
        }
    }

    @Test
    void testGetLexemeLeftParen() {
        try {
            Lexer lexer = new Lexer("(");
            Lexeme currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.LEFT_PAREN, currentLexeme.getType());
            assertEquals("(", currentLexeme.getLexemeText());
        } catch (PolynomialException i) {
            i.printStackTrace();
        }
    }

    @Test
    void testGetLexemeRightParen() {
        try {
            Lexer lexer = new Lexer(")");
            Lexeme currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.RIGHT_PAREN, currentLexeme.getType());
            assertEquals(")", currentLexeme.getLexemeText());
        } catch (PolynomialException i) {
            i.printStackTrace();
        }
    }

    @Test
    void testGetLexemeNumber() {
        try {
            Lexer lexer = new Lexer("123");
            Lexeme currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.NUMBER, currentLexeme.getType());
            assertEquals("123", currentLexeme.getLexemeText());
        } catch (PolynomialException i) {
            i.printStackTrace();
        }
    }

    @Test
    void testGetLexemeVariable() {
        try {
            Lexer lexer = new Lexer("a");
            Lexeme currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.VARIABLE, currentLexeme.getType());
            assertEquals("a", currentLexeme.getLexemeText());
        } catch (PolynomialException i) {
            i.printStackTrace();
        }
    }

    @Test
    void testGetLexemeExpression1() {
        try {
            Lexer lexer = new Lexer("123a+456");
            Lexeme currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.NUMBER, currentLexeme.getType());
            assertEquals("123", currentLexeme.getLexemeText());
            currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.VARIABLE, currentLexeme.getType());
            assertEquals("a", currentLexeme.getLexemeText());
            currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.PLUS, currentLexeme.getType());
            assertEquals("+", currentLexeme.getLexemeText());
            currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.NUMBER, currentLexeme.getType());
            assertEquals("456", currentLexeme.getLexemeText());
        } catch (PolynomialException i) {
            i.printStackTrace();
        }
    }

    @Test
    void testGetLexemeExpression2() {
        try {
            Lexer lexer = new Lexer("(a+b)^2");
            Lexeme currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.LEFT_PAREN, currentLexeme.getType());
            assertEquals("(", currentLexeme.getLexemeText());
            currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.VARIABLE, currentLexeme.getType());
            assertEquals("a", currentLexeme.getLexemeText());
            currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.PLUS, currentLexeme.getType());
            assertEquals("+", currentLexeme.getLexemeText());
            currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.VARIABLE, currentLexeme.getType());
            assertEquals("b", currentLexeme.getLexemeText());
            currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.RIGHT_PAREN, currentLexeme.getType());
            assertEquals(")", currentLexeme.getLexemeText());
            currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.CARET, currentLexeme.getType());
            assertEquals("^", currentLexeme.getLexemeText());
            currentLexeme = lexer.getLexeme();
            assertEquals(LexemeType.NUMBER, currentLexeme.getType());
            assertEquals("2", currentLexeme.getLexemeText());
        } catch (PolynomialException i) {
            i.printStackTrace();
        }
    }
}
